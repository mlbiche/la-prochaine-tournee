import {
  apiSpendingFromForm,
  fromApiSpending,
  toPriceString
} from '../lib/utils'

describe('toPrinceString', () => {
  test('it parses a number without digit to a 2-digit number as a string', () => {
    expect(toPriceString(2)).toBe('2,00')
  })

  test('it parses a number with 1 digit to a 2-digit number as a string', () => {
    expect(toPriceString(2.1)).toBe('2,10')
  })

  test('it parses a number with 2 digit to a 2-digit number as a string', () => {
    expect(toPriceString(2.12)).toBe('2,12')
  })

  test('it parses a number with more than 2 digit to a 2-digit number as a string', () => {
    expect(toPriceString(2.12345)).toBe('2,12')
  })
})

describe('fromApiSpending', () => {
  test('it transforms a spending from the Airtable API to a spending that can be used int eh application', () => {
    expect(
      fromApiSpending({
        id: 'test-id',
        author: 'mado',
        description: '🫒 My spending',
        price: 3,
        involved_guys: ['thibaud']
      })
    ).toEqual({
      id: 'test-id',
      author: 'mado',
      emoji: '🫒',
      description: 'My spending',
      price: 3,
      involved_guys: ['thibaud']
    })

    expect(
      fromApiSpending({
        id: 'test-id',
        author: 'mado',
        description: '🫒 My spending',
        price: 3,
        involved_guys: ['thibaud', 'mado']
      })
    ).toEqual({
      id: 'test-id',
      author: 'mado',
      emoji: '🫒',
      description: 'My spending',
      price: 3,
      involved_guys: ['thibaud', 'mado']
    })
  })
})

describe('apiSpendingFromForm', () => {
  test('it transforms a spending from the application to a spending that can be understood by the Airtable API', () => {
    expect(
      apiSpendingFromForm({
        id: 'test-id',
        author: 'mado',
        emoji: '🫒',
        description: 'My spending',
        price: 3,
        involved_guys: '["thibaud"]'
      })
    ).toEqual({
      id: 'test-id',
      author: 'mado',
      description: '🫒 My spending',
      price: 3,
      involved_guys: ['thibaud']
    })

    expect(
      apiSpendingFromForm({
        id: 'test-id',
        author: 'mado',
        emoji: '🫒',
        description: 'My spending',
        price: 3,
        involved_guys: '["mado", "thibaud"]'
      })
    ).toEqual({
      id: 'test-id',
      author: 'mado',
      description: '🫒 My spending',
      price: 3,
      involved_guys: ['mado', 'thibaud']
    })
  })
})
