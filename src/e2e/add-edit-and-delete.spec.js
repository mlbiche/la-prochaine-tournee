// import { expect, test } from '@playwright/test'

// test('user can add a spending from spending list', async ({ page }) => {
//   await page.goto('/la-prochaine-tournee')

//   await page.click('.add-button')

//   await expect(page).toHaveURL('la-prochaine-tournee/add')

//   const submitBtn = page.locator('.submit-btn')

//   await expect(submitBtn).toBeDisabled()

//   await page.selectOption('#spending-author', '🙋🏼‍♀️ Mado')
//   await page.fill('#spending-price', '10')
//   await page.fill('#spending-emoji', '🧪')
//   await page.fill('#spending-description', 'Test spending')
//   await page.selectOption('#spending-involved-guys', '👯 Tous les deux')

//   await expect(submitBtn).toBeEnabled()

//   await submitBtn.click()

//   await expect(page).toHaveURL(`la-prochaine-tournee`)

//   await expect(page.locator('.card-title').first()).toHaveText(
//     '🧪 Test spending 10,00€'
//   )

//   await expect(page.locator('.spending-detail').first()).toHaveText(
//     '🙋🏼‍♀️ Mado 👉 👯 Tous les deux'
//   )

//   await page.locator('text=✏️').first().click()

//   await expect(page).toHaveURL(/la-prochaine-tournee\/edit\/.*$/)

//   await expect(page.locator('#spending-author')).toHaveValue('🙋🏼‍♀️ Mado')
//   await expect(page.locator('#spending-price')).toHaveValue('10')
//   await expect(page.locator('#spending-emoji')).toHaveValue('🧪')
//   await expect(page.locator('#spending-description')).toHaveValue(
//     'Test spending'
//   )
//   await expect(page.locator('#spending-involved-guys')).toHaveValue(
//     '👯 Tous les deux'
//   )

//   await page.fill('#spending-price', '15')

//   await page.locator('.submit-btn').click()

//   await expect(page).toHaveURL(`la-prochaine-tournee`)

//   await expect(page.locator('.card-title').first()).toHaveText(
//     '🧪 Test spending 15,00€'
//   )

//   await page.locator('text=🗑').first().click()
//   await page.locator('text=👊 Ça vire !').first().click()

//   await expect(page.locator('.card-title').first()).not.toHaveText(
//     '🧪 Test spending 15,00€'
//   )
// })
