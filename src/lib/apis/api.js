import { fromApiSpending } from '$lib/utils'

const API_HOSTNAME = import.meta.env.VITE_API_HOSTNAME
const SPENDINGS_LIMIT = 9

const spendingCache = {}

export async function getSpendingList(fetch, page) {
  const params = new URLSearchParams({
    limit: SPENDINGS_LIMIT,
    offset: page * SPENDINGS_LIMIT
  })

  const res = await fetch(`${API_HOSTNAME}/spendings/?${params}`, {
    method: 'GET'
  })

  if (res.ok) {
    const { spendings, balance, count } = await res.json()
    return { spendings: spendings.map(s => fromApiSpending(s)), balance, count }
  } else throw new Error()
}

export async function addSpending(fetch, spending) {
  const res = await fetch(`${API_HOSTNAME}/spendings/`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(spending)
  })

  if (!res.ok) throw new Error()
}

export async function deleteSpending(fetch, spendingId) {
  const res = await fetch(`${API_HOSTNAME}/spendings/${spendingId}`, {
    method: 'DELETE'
  })

  if (!res.ok) throw new Error()

  return await res.json()
}

export async function getSpending(fetch, spendingId) {
  const spendingFromCache = spendingCache[spendingId]

  if (spendingFromCache) return spendingFromCache

  const res = await fetch(`${API_HOSTNAME}/spendings/${spendingId}`, {
    method: 'GET'
  })

  if (res.ok) return fromApiSpending(await res.json())

  throw new Error()
}

export async function updateSpending(fetch, spending) {
  const res = await fetch(`${API_HOSTNAME}/spendings/${spending.id}`, {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(spending)
  })

  if (res.ok) return await res.json()

  throw new Error()
}
