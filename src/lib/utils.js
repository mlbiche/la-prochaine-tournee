export function toPriceString(price) {
  const [integerPart, decimalPart] = price.toString().split('.')
  const firstDigit = decimalPart?.length > 0 ? decimalPart[0] : '0'
  const secondDigit = decimalPart?.length > 1 ? decimalPart[1] : '0'

  return `${integerPart},${firstDigit}${secondDigit}`
}

export function fromApiSpending({
  id,
  author,
  description,
  price,
  involved_guys
}) {
  return {
    id,
    author,
    emoji: description.split(' ')[0],
    description: description.split(' ').slice(1).join(' '),
    price,
    involved_guys
  }
}

export function apiSpendingFromForm(spending) {
  spending.description = `${spending.emoji} ${spending.description}`
  delete spending.emoji
  spending.involved_guys = JSON.parse(spending.involved_guys)

  return spending
}
