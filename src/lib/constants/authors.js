export const MADO = '🙋🏼‍♀️ Mado'
export const THIBAUD = '🙋🏽‍♂️ Thibaud'
const MADO_VALUE = 'mado'
const THIBAUD_VALUE = 'thibaud'

export const AUTHORS = {
  [MADO_VALUE]: MADO,
  [THIBAUD_VALUE]: THIBAUD
}
export const AUTHOR_VALUES = [MADO_VALUE, THIBAUD_VALUE]
export const BOTH_OF_US_LABEL = '👯 Tous les deux'
export const BOTH_OF_US_VALUE = JSON.stringify(AUTHOR_VALUES)
