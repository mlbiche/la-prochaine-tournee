# la-prochaine-tournee

_La prochaine tournée_ 🍻 is a tiny Web tool used for tracking common spending to decide who, from 🙋🏼‍♀️ Mado or 🙋🏽‍♂️ Thibaud, will pay the next round.

_La prochaine tournée_ is [live for 🙋🏼‍♀️ Mado or 🙋🏽‍♂️ Thibaud](https://mlbiche.gitlab.io/la-prochaine-tournee/) but you can get the hang on it on the [demo version](https://mlbiche.gitlab.io/la-prochaine-tournee-demo/).

## Dependencies

To run _La prochaine tournée_, you need:

- `node.js` _([download link](https://nodejs.org/en/))_. After the installation, check that `node` is properly installed by running in the terminal of your choice `node -v`. The command should display the `node` installed version.
- `pnpm` _([installation guide](https://pnpm.io/installation))_, a performant alternative to `npm`. You can also check it installed successfully by running `pnpm -v`.

And that's it! The project will install all the node.js module dependencies itself on the next step. 🤙

## Quick start

In the terminal of your choice, go through the following steps:

- Clone the project to the folder of your choice.
- Install the project: `pnpm install` _→ It installs all the needed node.js modules._

  **Note:** You only have to run this command the first time.

  **Note:** If you want to contribute to the project, please run `npm exec simple-git-hooks` so your code is linted when commiting.

- Add a _.env_ file with your configuration value. An example is provided in _.env.dist_ file.
- Start the project: `npm run dev` _→ It launches the Web application._

You're done! The Web Application is now accessible on http://localhost:3000/. 🚀

## Building the application

Then, you can build the Web application by simply running `npm run build`.

> You can preview the built app with `npm run preview`, regardless of whether you installed an adapter. This should _not_ be used to serve your app in production.

## Deploying the application

GitLab automatically runs tests, builds the application and deploys it on GitLab Pages when committing on the `main` branch.
