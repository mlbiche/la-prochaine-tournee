import adapter from '@sveltejs/adapter-static'
import autoprefixer from 'autoprefixer'
import sveltePreprocess from 'svelte-preprocess'

const dev = process.env.NODE_ENV === 'development'

/** @type {import('@sveltejs/kit').Config} */
const config = {
  kit: {
    adapter: adapter({
      pages: 'public'
    }),
    paths: {
      base: dev ? '' : '/la-prochaine-tournee'
    },
    prerender: {
      crawl: false // FIXME : See https://gitlab.com/mlbiche/la-prochaine-tournee/-/issues/25
    }
  },
  preprocess: sveltePreprocess({
    scss: {
      includePaths: ['src/styles', 'node_modules']
    },
    postcss: {
      plugins: [autoprefixer]
    }
  })
}

export default config
